import { createStackNavigator } from "react-navigation";
import Drawer from "./DrawerNavigator";
import Booth from "../containers/BoothContainer";
import Worker from "../containers/WorkerContainer";
import RNCameraContainer from "../containers/RNCameraContainer";

export default createStackNavigator(
  {
    Drawer: { screen: Drawer },
    Booth: { screen: Booth },
    Worker: { screen: Worker },
    RNCameraComponent: { screen: RNCameraContainer }
  },
  {
    initialRouteName: "Drawer",
    headerMode: "none"
  }
);
