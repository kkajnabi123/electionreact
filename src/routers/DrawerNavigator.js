import React from "react";
import { createDrawerNavigator } from "react-navigation";
import SideBar from "../containers/SlidebarContainer";
import Home from "../containers/HomeContainer";
import Modal from "../containers/ModalContainer";
import PersonalDetailsContainer from "../containers/personalDetailsContainer";
import IssueCapturingContainer from "../containers/IssueCapturingContainer";
import KeyContactsContainer from "../containers/KeyContactsContainer";
import MigratedVotersContainer from "../containers/MigratedVotersContainer";

export default createDrawerNavigator(
  {
    Home: { screen: Home },
    Modal: { screen: Modal },
    ProfileDetails: { screen: PersonalDetailsContainer },
    IssueCapturing: { screen: IssueCapturingContainer },
    MigratedVoters: { screen: MigratedVotersContainer },
    KeyContacts: { screen: KeyContactsContainer }
  },
  {
    initialRouteName: "Home",
    contentComponent: props => <SideBar {...props} />
  }
);
