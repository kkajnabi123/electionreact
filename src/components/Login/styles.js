export default {
  container: {
    backgroundColor: "#fff"
  },
  logo: {
    marginTop: 104,
    alignSelf: "center",
    width: 350,
    height: 100
  },
  textLogo: {
    marginTop: 18,
    alignSelf: "center",
    fontSize: 16
  },
  containerForm: {
    flex: 1,
    marginLeft: 32,
    marginRight: 32
  },
  contentForm: {
    marginTop: 5
  },
  buttonLogin: {
    marginTop: 50,
    backgroundColor: '#0266B4'
  },
  textLogin: {
    fontSize: 19
  },
  containerEnv: {
    alignItems: "center",
    marginTop: 20
  },
  textChangeEnv: {
    marginTop: 5
  }
};
