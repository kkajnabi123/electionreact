export default {
  container: {
    flex: 1,
    backgroundColor: "#F2F2F2",
  },
  drawerCover: {
    backgroundColor: "#0266B4",
    height: 104,
    flexDirection: "row",
    alignItems: "center",
  },
  avatar: {
    width: 56,
    height: 56,
    borderRadius: 28,
    borderWidth: 1,
    borderColor: "#64C8D0",
    marginLeft: 16
  },
  organizerName: {
    marginLeft: 16,
    fontSize: 17,
    fontWeight: "bold",
    marginRight: 16,
    flex: 1,
    color: '#fff'
  },
  menuItem: {
    height: 70
  },
  menuText: {
    marginLeft: 16,
    fontSize: 17
  },
  footer: {
    marginLeft: 16,
    flexDirection: "row",
    height: 70
  }
};
