import React, { Component } from "react";
import {
  Header, Left, Container, Button, Body, Title, Right, Icon, Text, Content
} from "native-base";
import styles from "./styles";
import { ScrollView, TouchableOpacity, StatusBar, View } from 'react-native';
import AlbumDetail from '../Base/albumDetail';
import Spinner from "react-native-loading-spinner-overlay";

export default class Home extends Component {

  renderAlbumList() {
    return this.props.mandals.map(mandal =>
      <TouchableOpacity key={mandal.mandalName} onPress={() => this.props.onPress(mandal)}>
        <View>
          <AlbumDetail album={mandal} constituencyData={this.props.constituencyData} isMicVisible={false} />
        </View>
      </TouchableOpacity>
    )
  }

  render() {
    return (
      <Container style={styles.container}>
        <StatusBar translucent={false} />
        {/* <SpinnerOverlay visible={this.props.loading} /> */}
        <Header
          androidStatusBarColor={"#0266B4"}
          style={{ borderBottomWidth: 0, backgroundColor: '#0266B4' }}>
          <Left style={styles.headerLeft}>
            <Button transparent onPress={() => this.props.navigation.openDrawer()}>
              <Icon name="menu" style={{ color: "#fff" }} />
            </Button>
          </Left>
          <Body style={styles.headerBody}>
            <Title style={styles.textBody}>{this.props.constituencyData.constituencyName}</Title>
          </Body>
          <Right style={styles.headerRight}>
          </Right>
        </Header>
        <Content style={{ marginTop: 5 }}>
          {/* <Text style={{ alignSelf: "center", marginTop: 10 }}>React Native Starter Kit v2.7.2</Text> */}
          {/* <BaseSpinner color='red' /> */}
          <ScrollView>
            {this.renderAlbumList()}
          </ScrollView >
        </Content>
      </Container>
    );
  }
}
