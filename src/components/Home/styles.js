export default {
  container: {
    flex: 1,
    backgroundColor: '#DCDCDC'
  },
  headerLeft: {
    flex: 0.3
  },
  headerBody: {
    flex: 0.4
  },
  textBody: {
    alignSelf: "center",
    color: "white"
  },
  headerRight: {
    flex: 0.3
  }
};
