import React, { Component } from "react";
import {
    Header, Left, Container, Button, Body, Title, Right, Icon, Text, Content, Textarea, Toast
} from "native-base";
import styles from "./styles";
import { ScrollView, TouchableOpacity, StatusBar, View, Modal, TouchableWithoutFeedback } from 'react-native';
import Spinner from "react-native-loading-spinner-overlay";

export default class IssueCapturing extends Component {

    state = {
        message: ''
    };

    render() {
        return (
            <Container style={styles.container}>
                <StatusBar translucent={false} />
                {/* <Spinner visible={this.props.loading} /> */}
                <Header
                    androidStatusBarColor={"#0266B4"}
                    style={{ borderBottomWidth: 0, backgroundColor: '#0266B4' }}>
                    <Left style={styles.headerLeft}>
                        {/* <Button transparent onPress={() => this.props.navigation.openDrawer()}> */}
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name="arrow-back" style={{ color: "#fff" }} />
                        </Button>
                    </Left>
                    <Body style={styles.headerBody}>
                        <Title style={styles.textBody}>Issue Capturing</Title>
                    </Body>
                    <Right style={styles.headerRight} />
                </Header>
                <Content style={{ margin: 10 }}>
                    {/* <Text style={{ alignSelf: "center", marginTop: 10 }}>React Native Starter Kit v2.7.2</Text> */}
                    {/* <ScrollView>
                        {this.renderAlbumList()}
                    </ScrollView > */}

                    <Textarea rowSpan={5} bordered placeholder="Enter Your Issue Here..."
                        onChangeText={(text) => this.setState({ message: text })} value={this.state.message}
                    />

                    <Button full rounded iconRight style={{ marginTop: 10, backgroundColor: '#0266B4' }} onPress={() => {
                        this.props.onCameraPress();
                    }}>
                        <Text>Attach Image</Text>
                        <Icon name='md-attach' />
                    </Button>

                    <Button full rounded style={{ marginTop: 10, backgroundColor: '#0266B4' }}
                        onPress={() => {
                            Toast.show({
                                text: 'Successfully submitted !!!!'
                            });
                            this.setState({ message: '' })
                        }}>
                        <Text>Submit</Text>
                    </Button>
                </Content>
            </Container>
        );
    }
}
