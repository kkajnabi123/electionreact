export default {
    modal: {
        backgroundColor: 'white',
        alignItems: undefined,
        justifyContent: undefined
    },
    buttonStyle: {
        alignSelf: 'stretch',
        backgroundColor: '#2980B9'
    },
    textStyle: {
        alignSelf: 'center',
        color: '#ffffff',
        fontSize: 16,
        fontWeight: '600',
        paddingTop: 10,
        paddingBottom: 10
    },
    textBody: {
        color: '#ffffff'
    }
};
