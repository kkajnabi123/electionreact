import React, { Component } from "react";
import {
    Header, Left, Container, Button, Body, Title, Right, Icon, Text, Content, Textarea
} from "native-base";
import styles from "./styles";
import { ScrollView, TouchableOpacity, StatusBar, View, Modal, TouchableWithoutFeedback } from 'react-native';
import Spinner from "react-native-loading-spinner-overlay";

export default class MessagePopUp extends Component {

    state = {
        message: ''
    };

    render() {
        return (
            <Modal
                style={styles.modal}
                animationType="fade"
                transparent={true}
                visible={this.props.isModalPresent}
                onRequestClose={() => {
                    alert('Modal has been closed.');
                }}>
                <View style={{
                    flex: 1,
                    flexDirection: 'column',
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: '#00000080'
                }}>
                    <View style={{
                        width: '80%',
                        height: '50%',
                        backgroundColor: '#fff'
                    }}>
                        <Header
                            androidStatusBarColor={"#0266B4"}
                            style={{ borderBottomWidth: 0, backgroundColor: '#0266B4' }}>
                            {/* <Left style={styles.headerLeft}>
                                <Button transparent onPress={() => this.props.navigation.openDrawer()}>
                                    <Button transparent onPress={() => this.props.navigation.goBack()}>
                                        <Icon name="arrow-back" style={{ color: "#fff" }} />
                                    </Button> 
                            </Left> */}
                            <Body style={styles.headerBody}>
                                <Title style={styles.textBody}>Message</Title>
                            </Body>
                            <Right style={styles.headerRight}>
                                <TouchableWithoutFeedback transparent onPress={this.props.setModalVisible}>
                                    <View style={{ marginRight: 15 }}>
                                        <Icon name="close" style={{ color: "#fff" }} size={30} />
                                    </View>
                                </TouchableWithoutFeedback>
                            </Right>
                        </Header>
                        <Content style={{ margin: 10 }}>
                            <Textarea rowSpan={5} bordered placeholder="Enter Your Message Here..."
                                onChangeText={(text) => this.setState({ message: text })} value={this.state.message}
                            />
                            {/* <ScrollView>
                        {this.renderAlbumList()}
                    </ScrollView > */}
                        </Content>
                        <View style={{ flexDirection: "row" }}>
                            <View style={{ flex: 1 }}>
                                <TouchableOpacity style={styles.buttonStyle} onPress={() => this.props.onSavePress(this.state.message)}>
                                    <Text style={styles.textStyle}>Send</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>
            </Modal >
        );
    }
}
