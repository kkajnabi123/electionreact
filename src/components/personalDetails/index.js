import React, { Component } from "react";
import {
    Header, Left, Container, Button, Body, Title, Right, Icon, Text, Content, Textarea, Toast
} from "native-base";
import styles from "./styles";
import { ScrollView, TextInput, TouchableOpacity, StatusBar, View, Modal, TouchableWithoutFeedback } from 'react-native';
import Spinner from "react-native-loading-spinner-overlay";

export default class PersonalDetails extends Component {

    state = {
        ysrcpText: '',
        tdpText: '',
        jansenaText: '',
        othersText: ''
    };

    render() {
        return (
            <Container style={styles.container}>
                <StatusBar translucent={false} />
                {/* <Spinner visible={this.props.loading} /> */}
                <Header
                    androidStatusBarColor={"#0266B4"}
                    style={{ borderBottomWidth: 0, backgroundColor: '#0266B4' }}>
                    <Left style={styles.headerLeft}>
                        {/* <Button transparent onPress={() => this.props.navigation.openDrawer()}> */}
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name="arrow-back" style={{ color: "#fff" }} />
                        </Button>
                    </Left>
                    <Body style={styles.headerBody}>
                        <Title style={styles.textBody}>Voter's Mood</Title>
                    </Body>
                    <Right style={styles.headerRight} />
                </Header>
                <Content style={{ margin: 5 }}>
                    <Text style={{ alignSelf: "center", marginTop: 10, marginBottom: 10 }}>( 1-100 )</Text>
                    <TextInput
                        style={{ height: 40, borderColor: 'gray', borderWidth: 1 }}
                        placeholder="YSRCP"
                        onChangeText={(text) => this.setState({ ysrcpText: text })} value={this.state.ysrcpText}
                    />
                    <TextInput
                        style={{ height: 40, borderColor: 'gray', borderWidth: 1 }}
                        placeholder="TDP"
                        onChangeText={(text) => this.setState({ tdpText: text })} value={this.state.tdpText}
                    />
                    <TextInput
                        style={{ height: 40, borderColor: 'gray', borderWidth: 1 }}
                        placeholder="JAN SENA"
                        onChangeText={(text) => this.setState({ jansenaText: text })} value={this.state.jansenaText}
                    />
                    <TextInput
                        style={{ height: 40, borderColor: 'gray', borderWidth: 1 }}
                        placeholder="Others"
                        onChangeText={(text) => this.setState({ othersText: text })} value={this.state.othersText}
                    />

                    <Button full rounded style={{ marginTop: 10, backgroundColor: '#0266B4' }}
                        onPress={() => {
                            Toast.show({
                                text: 'Successfully submitted !!!!'
                            });
                            this.setState({ ysrcpText: '', tdpText: '', jansenaText: '', othersText: '' })
                        }}>
                        <Text>Submit</Text>
                    </Button>
                </Content>
            </Container>
        );
    }
}
