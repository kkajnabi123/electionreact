import React, { Component } from "react";
import {
  Header, Left, Container, Button, Body, Title, Right, Icon, Text, Content, Fab, Card, CardItem, Spinner as BaseSpinner
} from "native-base";
import styles from "./styles";
import { ScrollView, TouchableOpacity, StatusBar, View, TouchableWithoutFeedback } from 'react-native';
import Spinner from "react-native-loading-spinner-overlay";
import MessagePopUpContainer from '../../containers/MessagePopUpContainer';

export default class Worker extends Component {

  renderAlbumList() {
    return this.props.workers.map(worker =>
      <TouchableWithoutFeedback key={worker.contactNo} >
        <View>
          <Card style={this.renderCardStyle(worker)}>
            <CardItem style={styles.cardItemStyle}>
              <View style={styles.headerContent_style}>
                <Text style={styles.header_text_style}>{worker.mandalName ? worker.mandalName : (worker.memberName ? worker.memberName : worker.boothName)}</Text>
                <Text style={{ fontSize: 13 }}>{worker.convenor ? worker.convenor : worker.contactNo}</Text>
              </View>
              <View style={styles.micAndSpeakerbuttonStyle}>
                <TouchableOpacity onPress={() => this.props.onSpeakerPress(worker)}>
                  {this.renderSpeakerIcon(worker.isDeafEnabled)}
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.props.onMicPress(worker)}>
                  {this.renderMicIcon(worker.isMuteEnabled)}
                </TouchableOpacity>
              </View>
            </CardItem>
          </Card>
        </View>
      </TouchableWithoutFeedback>
    )
  }

  renderCardStyle(worker) {
    if (worker.ConferenceMemberID) {
      return styles.cardViewStyleGreen;
    }
    return styles.cardViewStyleRed;
  }

  renderMicIcon(isMuteEnabled) {
    if (isMuteEnabled) {
      return (<Icon name="mic" style={{ color: "green", marginLeft: 2 }} size={30} />);
    }

    return (<Icon name="mic-off" style={{ color: "red", marginRight: 2 }} size={30} />);
  }

  renderHeaderMicIcon(isMuteEnabled) {
    if (isMuteEnabled) {
      return (<Icon name="mic" style={{ color: "#fff", marginLeft: 3, marginRight: 2 }} size={30} />);
    }

    return (<Icon name="mic-off" style={{ color: "#fff" }} size={30} />);
  }

  renderSpeakerIcon(isDeafEnabled) {

    console.log(isDeafEnabled);

    if (isDeafEnabled) {
      return (<Icon name="volume-up" style={{ color: "green" }} size={30} />);
    }

    return (<Icon name="volume-off" style={{ color: "red" }} size={30} />);
  }

  renderHeaderSpeakerIcon(isDeafEnabled) {
    if (isDeafEnabled) {
      return (<Icon name="volume-up" style={{ color: "#fff" }} size={30} />);
    }

    return (<Icon name="volume-off" style={{ color: "#fff" }} size={30} />);
  }

  renderCallLoadingIcon(isCallLoadingVisible) {
    if (isCallLoadingVisible) {
      return (<BaseSpinner color='#fff' />);
    }

    return (<Icon name="call" size={30} color="#fff" />);
  }

  render() {
    return (
      < Container style={styles.container} >
        <StatusBar translucent={false} />
        <Spinner visible={this.props.loading} />
        <Header
          androidStatusBarColor={"#0266B4"}
          style={{ borderBottomWidth: 0, backgroundColor: '#0266B4' }}>
          <Left style={styles.headerLeft}>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back" style={{ color: "#fff" }} />
            </Button>
          </Left>
          <Body style={styles.headerBody}>
            <Title style={styles.textBody}>{this.props.selectedBooth.boothName}</Title>
          </Body>
          <Right style={styles.headerRight}>
            <TouchableWithoutFeedback transparent onPress={() => this.props.onHeaderSpeakerPress()}>
              <View style={{ marginRight: 15 }}>
                {this.renderHeaderSpeakerIcon(this.props.headerSpeakerIcon)}
              </View>
            </TouchableWithoutFeedback>
            <TouchableWithoutFeedback transparent onPress={() => this.props.onHeaderMicPress()}>
              <View style={{ marginRight: 8 }}>
                {this.renderHeaderMicIcon(this.props.headerMicIcon)}
              </View>
            </TouchableWithoutFeedback>
          </Right>
        </Header>
        <View style={{
          flexDirection: 'row', justifyContent: 'space-between', elevation: 5,
          position: 'relative', backgroundColor: '#ffffff', height: 20
        }}>
          <View style={{ flexDirection: 'row', marginLeft: 10 }}>
            <View style={{ width: 10, height: 10, backgroundColor: '#008000', marginTop: 5, marginRight: 5 }} /><Text style={{ fontSize: 13 }}>Connected {this.props.noOfConnected}</Text>
          </View>
          <View style={{ flexDirection: 'row', marginRight: 10 }}>
            <View style={{ width: 10, height: 10, backgroundColor: '#FF0000', marginTop: 5, marginRight: 5 }} /><Text style={{ fontSize: 13 }}>Disconnected {this.props.noOfDisConnected}</Text>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ width: 10, height: 10, backgroundColor: '#DCDCDC', marginTop: 5, marginRight: 5 }} /><Text style={{ fontSize: 13, marginRight: 8 }}>Total {this.props.workers.length}</Text>
          </View>
        </View>
        <Content style={{ marginTop: 5 }}>
          {/* <Text style={{ alignSelf: "center", marginTop: 10 }}>React Native Starter Kit v2.7.2</Text> */}
          {
            this.props.modalVisible ?
              <MessagePopUpContainer isModalPresent={this.props.modalVisible} onSavePress={(data) => this.props.onSavePress(data)}
                setModalVisible={() => {
                  this.props.setModalVisible()
                }} /> :
              null
          }
          <ScrollView>
            {this.renderAlbumList()}
          </ScrollView >
        </Content>
        <Fab
          containerStyle={{}}
          style={{ backgroundColor: '#008E46' }}
          position="bottomRight"
          onPress={() => {
            if (!this.props.isCallLoadingVisible) {
              this.props.onCallPress()
            }
          }}>
          {this.renderCallLoadingIcon(this.props.isCallLoadingVisible)}
        </Fab>
        <Fab
          containerStyle={{ marginBottom: 60 }}
          style={{ backgroundColor: '#008E46' }}
          position="bottomRight"
          onPress={() => this.props.onMessagePress()}>
          <Icon name="mail" size={30} color="#fff" />
        </Fab>
      </Container >
    );
  }
}
