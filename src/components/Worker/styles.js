export default {
  container: {
    flex: 1,
    backgroundColor: '#DCDCDC'
  },
  headerLeft: {
    flex: 0.3
  },
  headerBody: {
    flex: 0.4
  },
  textBody: {
    alignSelf: "center",
    color: "white"
  },
  headerRight: {
    flex: 0.3
  },
  cardItemStyle: {
    flexDirection: "row",
    alignItems: "center",
    paddingRight: 0
  },
  icon_style_mic: {
    alignItems: 'flex-end',
    flex: 1,
    marginRight: -20
  },
  icon_style_speaker: {
    alignItems: 'flex-end',
    flex: 1
  },
  micAndSpeakerbuttonStyle: {
    flexDirection: "row",
    justifyContent: 'flex-end',
    flex: 1
  },
  cardViewStyleRed: {
    marginLeft: 8,
    marginRight: 8,
    borderLeftWidth: 10,
    borderColor: 'red',
    borderRadius: 10
  },
  cardViewStyleGreen: {
    marginLeft: 8,
    marginRight: 8,
    borderLeftWidth: 10,
    borderColor: 'green',
    borderRadius: 10
  },
  headerContent_style: {
    justifyContent: 'space-around',
    flexDirection: 'column'
  },
  header_text_style: {
    fontSize: 16
  }
};
