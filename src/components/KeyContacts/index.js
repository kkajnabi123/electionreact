import React, { Component } from "react";
import {
    Header, Left, Container, Button, Body, Title, Right, Icon, Text, Content, Textarea, Toast
} from "native-base";
import styles from "./styles";
import { ScrollView, TextInput, TouchableOpacity, StatusBar, View, Modal, TouchableWithoutFeedback, Picker } from 'react-native';
import Spinner from "react-native-loading-spinner-overlay";

export default class KeyContacts extends Component {

    state = {
        name: '',
        contact: ''
    };

    render() {
        return (
            <Container style={styles.container}>
                <StatusBar translucent={false} />
                {/* <Spinner visible={this.props.loading} /> */}
                <Header
                    androidStatusBarColor={"#0266B4"}
                    style={{ borderBottomWidth: 0, backgroundColor: '#0266B4' }}>
                    <Left style={styles.headerLeft}>
                        {/* <Button transparent onPress={() => this.props.navigation.openDrawer()}> */}
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name="arrow-back" style={{ color: "#fff" }} />
                        </Button>
                    </Left>
                    <Body style={styles.headerBody}>
                        <Title style={styles.textBody}>Key Contacts</Title>
                    </Body>
                    <Right style={styles.headerRight} />
                </Header>
                <Content style={{ margin: 5 }}>
                    {/* <Text style={{ alignSelf: "center", marginTop: 10, marginBottom: 10 }}>( 1-100 )</Text> */}
                    <Picker
                        selectedValue={this.state.language}
                        style={{ height: 50, flex: 1, borderWidth: 1 }}
                        onValueChange={(itemValue, itemIndex) =>
                            this.setState({ language: itemValue })
                        }>
                        <Picker.Item label="YSRCP" value="ysrcp" />
                        <Picker.Item label="TDP" value="tdp" />
                        <Picker.Item label="JAN SENA" value="jan sena" />
                        <Picker.Item label="NEUTRAL" value="neutral" />
                    </Picker>
                    <TextInput
                        style={{ height: 40, borderColor: 'gray', borderWidth: 1 }}
                        placeholder="Full Name"
                        onChangeText={(text) => this.setState({ name: text })} value={this.state.name}
                    />
                    <TextInput
                        style={{ height: 40, borderColor: 'gray', borderWidth: 1 }}
                        placeholder="Contact Details"
                        onChangeText={(text) => this.setState({ contact: text })} value={this.state.contact}
                    />

                    <Button full rounded style={{ marginTop: 10, backgroundColor: '#0266B4' }}
                        onPress={() => {
                            Toast.show({
                                text: 'Successfully submitted !!!!'
                            });
                            this.setState({ name: '', contact: '' })
                        }}>
                        <Text>Submit</Text>
                    </Button>
                </Content>
            </Container>
        );
    }
}
