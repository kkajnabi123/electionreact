import React, { Component } from "react";
import {
  Header, Left, Container, Button, Body, Title, Right, Icon, Text, Content
} from "native-base";
import styles from "./styles";
import { ScrollView, TouchableOpacity, StatusBar, View } from 'react-native';
import AlbumDetail from '../Base/albumDetail';
import Spinner from "react-native-loading-spinner-overlay";

export default class Booth extends Component {

  renderAlbumList() {
    return this.props.booths.map(booth =>
      <TouchableOpacity key={booth.boothName} onPress={() => this.props.onPress(booth)}>
        <View>
          <AlbumDetail album={booth} isMicVisible={false} />
        </View>
      </TouchableOpacity>
    )
  }

  render() {
    return (
      <Container style={styles.container}>
        <StatusBar translucent={false} />
        <Spinner visible={this.props.loading} />
        <Header
          androidStatusBarColor={"#0266B4"}
          style={{ borderBottomWidth: 0, backgroundColor: '#0266B4' }}>
          <Left style={styles.headerLeft}>
            {/* <Button transparent onPress={() => this.props.navigation.openDrawer()}> */}
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back" style={{ color: "#fff" }} />
            </Button>
          </Left>
          <Body style={styles.headerBody}>
            <Title style={styles.textBody}>{this.props.selectedMandal.mandalName}</Title>
          </Body>
          <Right style={styles.headerRight} />
        </Header>
        <Content style={{ marginTop: 5 }}>
          {/* <Text style={{ alignSelf: "center", marginTop: 10 }}>React Native Starter Kit v2.7.2</Text> */}
          <ScrollView>
            {this.renderAlbumList()}
          </ScrollView >
        </Content>
      </Container>
    );
  }
}
