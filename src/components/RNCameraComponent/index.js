import React, { Component } from "react";
import {
    Header, Left, Container, Button, Body, Title, Right, Icon, Text, Content, Textarea
} from "native-base";
import styles from "./styles";
import { ScrollView, TouchableOpacity, StatusBar, View, Modal, TouchableWithoutFeedback } from 'react-native';
import Spinner from "react-native-loading-spinner-overlay";
import { RNCamera } from 'react-native-camera';

export default class RNCameraComponent extends Component {

    state = {
        message: ''
    };

    render() {
        return (
            <View style={styles.container}>
                <RNCamera
                    ref={ref => {
                        this.camera = ref;
                    }}
                    style={styles.preview}
                    type={RNCamera.Constants.Type.back}
                    flashMode={RNCamera.Constants.FlashMode.on}
                    permissionDialogTitle={'Permission to use camera'}
                    permissionDialogMessage={'We need your permission to use your camera phone'}
                    onGoogleVisionBarcodesDetected={({ barcodes }) => {
                        console.log(barcodes);
                    }}
                />
                <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={this.takePicture.bind(this)} style={styles.capture}>
                        <Text style={{ fontSize: 14 }}> SNAP </Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

    takePicture = async function () {
        if (this.camera) {
            const options = { quality: 0.5, base64: true };
            const data = await this.camera.takePictureAsync(options);
            console.log("----------------" + data.uri);
            this.props.cameraRolled(data.uri);
        }
    };
}
