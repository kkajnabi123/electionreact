import React, { Component } from 'react';
import { Text, View, Image, StyleSheet, Linking } from 'react-native';
// import Card from './card';
// import CardSection from './cardSection';
import Button from './Button'
import { Container, Header, Content, Card, CardItem, Body, Icon } from 'native-base';

export default class AlbumDetail extends Component {

    render() {
        const { mandalName, contactNo, convenor, boothName, memberName } = this.props.album;
        const { headerContent_style,
            thumbnail_style,
            thumbnail_container_style,
            header_text_style,
            cardViewStyle,
            cardItemStyle,
            icon_style,
            art_work_style } = styles;

        return (
            <Card style={cardViewStyle}>
                <CardItem style={cardItemStyle} bordered first>
                    <View style={headerContent_style}>
                        <Text style={header_text_style}>{mandalName ? mandalName : (memberName ? memberName : boothName)}</Text>
                        <Text>{convenor ? convenor : contactNo} - {contactNo}</Text>
                    </View>
                    {this.renderMicIcon(icon_style)}
                </CardItem>
            </Card>
        );
    }

    renderMicIcon(icon_style) {
        if (this.props.isMicVisible) {
            return (
                <View style={icon_style}>
                    <Icon name="mic" size={30} />
                </View>
            );
        }
    }

}


const styles = StyleSheet.create({
    headerContent_style: {
        justifyContent: 'space-around',
        flexDirection: 'column'
    },
    header_text_style: {
        fontSize: 18,
        fontWeight: 'bold'
    },
    thumbnail_style: {
        height: 50,
        width: 50
    },
    thumbnail_container_style: {
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 10,
        marginRight: 10
    },
    art_work_style: {
        height: 300,
        flex: 1,
        width: null
    },
    cardViewStyle: {
        height: undefined,
        width: undefined,
        marginLeft: 8,
        marginRight: 8,
        borderWidth: 5,
        borderRadius: 10
    },
    cardItemStyle: {
        flexDirection: "row",
        alignItems: "center"
    },
    icon_style: {
        alignItems: 'flex-end',
        flex: 1,
        marginRight: -10
    }
})