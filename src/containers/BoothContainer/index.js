import React, { Component } from "react";
import Booth from "../../components/Booth";
import axios from 'axios';
import { AsyncStorage } from "react-native"

export default class BoothContainer extends Component {

  constructor(props) {
    super(props);
    this.loading = false;
  }

  state = {
    booths: [],
    selectedMandal: {},
    userData: {}
  };

  componentWillMount() {
    this.getUserDataFromDB();
  }

  async getUserDataFromDB() {

    console.log("333333333333");

    try {
      console.log("2222222222222");

      let value = await AsyncStorage.getItem('userData');
      if (value !== null) {
        console.log("===== 11", value);
        value = JSON.parse(value);
        console.log("===== 22", value);
        this.setState({ userData: value });
      }
    } catch (error) {
    }

    console.log("=====", this.state.userData);


    this.loading = true;
    const selectedMandal = this.props.navigation.getParam('selectedMandal', null);
    this.setState({ selectedMandal: selectedMandal });
    axios.get(`http://35.229.17.101:3789/boothmanagement/services/rest/getAllBoothsByMandal/${this.state.userData.authUser.constituencyId}/${selectedMandal.mandalId}`).then((response) => {
      console.log("response ===>>>" + JSON.stringify(response));
      this.loading = false;
      this.setState({ booths: response.data.boothData[0].members })
    }).catch(error => this.loading = false);
  }

  render() {
    return (
      <Booth navigation={this.props.navigation} loading={this.loading} booths={this.state.booths} selectedMandal={this.state.selectedMandal} onPress={(booth) => {
        this.props.navigation.navigate("Worker", { selectedBooth: booth, boothList: this.state.booths });
      }
      } />
    );
  }
}

