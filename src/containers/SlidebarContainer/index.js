import React, { Component } from "react";
import Sidebar from "../../components/sidebar";
import { connect } from "react-redux";
import { userRequestLogout } from "../../actions";

class SidebarContainer extends Component {
  constructor(props) {
    super(props);
    this.data = [
      {
        name: "Booth Committee",
        route: "Home",
        icon: "ios-contact"
      },
      {
        name: "Convenor Committee",
        route: "Home",
        icon: "ios-contacts"
      },
      {
        name: "Voter's Mood",
        route: "ProfileDetails",
        icon: "md-analytics"
      },
      {
        name: "Issue Capturing",
        route: "IssueCapturing",
        icon: "ios-bug"
      },
      {
        name: "Key Contacts",
        route: "KeyContacts",
        icon: "contacts"
      },
      {
        name: "Migrated Voters",
        route: "MigratedVoters",
        icon: "book"
      },
      {
        name: "Logout",
        route: "Logout",
        icon: "log-out"
      }
    ];
  }

  navigator(data) {
    if (data.route === "Logout") {
      this.props.logout();
      this.props.navigation.navigate("Login");
    } else {
      this.props.navigation.navigate(data.route);
    }
  }

  render() {
    return (
      <Sidebar
        data={this.data}
        onPress={(data) => this.navigator(data)} />
    );
  }
}

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(userRequestLogout())
});

export default connect(
  null,
  mapDispatchToProps
)(SidebarContainer);
