import React, { Component } from "react";
import PropTypes from "prop-types";
import { Image, Alert, AsyncStorage } from "react-native";
import { Item, Input, Toast, Form } from "native-base";
import { connect } from "react-redux";
import RNCameraComponent from "../../components/RNCameraComponent";
import { imageCaptureSuccess } from "../../actions";

class RNCameraContainer extends Component {

    constructor(props) {
        super(props);
        this.state = {
        };
    }

    componentWillMount() {

    }

    render() {
        return (
            <RNCameraComponent navigation={this.props.navigation}
                cameraRolled={(data) => {
                    console.log("===============>", data);
                    // this.props.imageCaptured({ data });
                    this.props.navigation.goBack();
                }}
            />
        );
    }
}

const mapStateToProps = state => ({
    uri: state.uri
});

const mapDispatchToProps = dispatch => ({
    imageCaptured: ({ uri }) => {
        dispatch(imageCaptureSuccess({
            uri
        })
        )

        console.log("hiiiiiiii", imageCaptureSuccess);

    }
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RNCameraContainer);
