import React, { Component } from "react";
import Home from "../../components/Home";
import axios from 'axios';
import { AsyncStorage } from "react-native"

export default class HomeContainer extends Component {

  constructor(props) {
    super(props);
    this.loading = false;
  }

  state = {
    mandals: [],
    constituencyData: {},
    userData: {}
  };

  componentWillMount() {
    this.getUserDataFromDB();
  }

  async getUserDataFromDB() {
    try {
      let value = await AsyncStorage.getItem('userData');
      if (value !== null) {
        value = JSON.parse(value);
        this.setState({ userData: value });
      }
    } catch (error) {
    }

    this.loading = true;
    axios.get(`http://35.229.17.101:3789/boothmanagement/services/rest/getConstituencyFromDB/${this.state.userData.authUser.constituencyId}`).then((response) => {
      // console.log("response ===>>>" + JSON.stringify(response));
      this.loading = false;
      this.setState({ mandals: response.data.ConstituencyData[0].mandalList, constituencyData: response.data.ConstituencyData[0] })
    }).catch(error => this.loading = false);

  }

  render() {
    return (
      <Home navigation={this.props.navigation} loading={this.loading} mandals={this.state.mandals} constituencyData={this.state.constituencyData} onPress={(mandal) => {
        // console.log('workssss' + mandalId);
        this.props.navigation.navigate("Booth", { selectedMandal: mandal });
      }
      } />
    );
  }
}

