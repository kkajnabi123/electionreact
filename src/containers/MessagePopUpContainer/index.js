import React, { Component } from "react";
import MessagePopUp from "../../components/MessagePopUp";
import axios from 'axios';

export default class MessagePopUpContainer extends Component {

    constructor(props) {
        super(props);
        this.loading = false;
    }

    state = {
        booths: [],
        selectedMandal: {}
    };

    componentWillMount() {
        // this.loading = true;
        // const selectedMandal = this.props.navigation.getParam('selectedMandal', null);
        // this.setState({ selectedMandal: selectedMandal });
        // axios.get('http://35.229.17.101:3789/boothmanagement/services/rest/getAllBoothsByMandal/1/' + selectedMandal.mandalId).then((response) => {
        //     // console.log("response ===>>>" + JSON.stringify(response));
        //     this.loading = false;
        //     this.setState({ booths: response.data.boothData[0].members })
        // }).catch(error => this.loading = false);
    }

    render() {
        return (
            <MessagePopUp isModalPresent={this.props.modalVisible}
                onSavePress={(data) => {
                    console.log("=====999999>>>");
                    this.props.onSavePress(data);
                }}
                setModalVisible={() => {
                    this.props.setModalVisible()
                }} />
        );
    }
}

