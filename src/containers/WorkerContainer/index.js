import React, { Component } from "react";
import Worker from "../../components/Worker";
import axios from 'axios';
import SocketIOClient from 'socket.io-client';
import { AsyncStorage } from "react-native"

export default class WorkerContainer extends Component {

  constructor(props) {
    super(props);
    this.socket = null;
  }

  state = {
    workers: [],
    selectedBooth: {},
    headerMicIcon: false,
    headerSpeakerIcon: true,
    noOfConnected: 0,
    noOfDisConnected: 0,
    userDataWithUUID: "",
    modalVisible: false,
    loading: false,
    loginData: {},
    originalWorkers: {},
    isCallLoadingVisible: false
  };

  componentWillMount() {
    const selectedBooth = this.props.navigation.getParam('selectedBooth', null);
    const boothList = this.props.navigation.getParam('boothList', null);

    AsyncStorage.getItem('userDataWithUUID').then((userDataWithUUID) => {
      // console.log("get userDataWithUUID" + userDataWithUUID);
      AsyncStorage.getItem('userData').then((userData) => {
        // console.log("userData" + userData);
        this.setState({ loginData: JSON.parse(userData), selectedBooth: selectedBooth })
        if (userDataWithUUID) {
          this.connectSocketAndFetchData(userDataWithUUID);
        }
      }).catch((err) => { })
    }).catch((err) => { })

    boothList.forEach(element => {
      if (element.boothId == selectedBooth.boothId) {

        element.ListOfWorkers.forEach(elementY => {
          elementY.isMuteEnabled = false;
          elementY.isDeafEnabled = true;
        });
        this.setState({ originalWorkers: JSON.parse(JSON.stringify(element.ListOfWorkers)) });
        this.setState({ workers: element.ListOfWorkers });
      }
    });
  }

  setModalVisible() {
    this.setState({ modalVisible: false })
  }

  onSavePress(data) {
    console.log("data ==>" + data);

    let memberIdList = [];
    let newWorkers = JSON.parse(JSON.stringify(this.state.workers));
    newWorkers.forEach((tempWorker) => {
      memberIdList.push(tempWorker.contactNo)
    });

    let body = {
      "confRoomName": this.state.loginData.contactNo,
      "memberIdList": memberIdList,
      "message": data
    }
    this.setState({ loading: true });
    axios.post('http://35.229.17.101:3989/sendMessageurl', body).then((response) => {
      this.setState({ modalVisible: false, loading: false });
    }).catch(error => this.setState({ loading: false }));
  }

  render() {
    return (
      <Worker navigation={this.props.navigation} workers={this.state.workers} headerMicIcon={this.state.headerMicIcon}
        noOfConnected={this.state.noOfConnected} selectedBooth={this.state.selectedBooth} headerSpeakerIcon={this.state.headerSpeakerIcon}
        noOfDisConnected={this.state.noOfDisConnected} loading={this.state.loading} modalVisible={this.state.modalVisible}
        setModalVisible={() => this.setModalVisible()}
        onSavePress={(data) => this.onSavePress(data)}
        isCallLoadingVisible={this.state.isCallLoadingVisible}
        onMicPress={(worker) => {

          let memberIdList = [];
          memberIdList.push(worker.ConferenceMemberID);
          let body = {
            confRoomName: this.state.userDataWithUUID,
            memberIdList: memberIdList
          }

          if (worker.isMuteEnabled) {
            this.setState({ loading: true });
            axios.post('http://35.229.17.101:3989/muteConferenceurl', body).then((response) => {
              this.setState({ loading: false })
              console.log("muteConferenceurl ==== >>" + JSON.stringify(response));
              worker.isMuteEnabled = false;
              let newWorkers = JSON.parse(JSON.stringify(this.state.workers))

              this.state.workers.forEach((tempWorker, index) => {
                if (tempWorker._id == worker._id) {
                  newWorkers[index] = worker;
                }
              });

              this.setState({ workers: newWorkers })
            }).catch(error => this.setState({ loading: false }));
          } else {
            this.setState({ loading: true });
            axios.post('http://35.229.17.101:3989/unMuteConferenceurl', body).then((response) => {
              this.setState({ loading: false })
              worker.isMuteEnabled = true;
              let newWorkers = JSON.parse(JSON.stringify(this.state.workers))

              this.state.workers.forEach((tempWorker, index) => {
                if (tempWorker._id == worker._id) {
                  newWorkers[index] = worker;
                }
              });

              this.setState({ workers: newWorkers })
            }).catch(error => this.setState({ loading: false }));
          }
        }
        }

        onSpeakerPress={(worker) => {

          let memberIdList = [];
          memberIdList.push(worker.ConferenceMemberID);
          let body = {
            confRoomName: this.state.userDataWithUUID,
            memberIdList: memberIdList
          }

          if (worker.isDeafEnabled) {
            this.setState({ loading: true });
            axios.post('http://35.229.17.101:3989/deafConferenceurl', body).then((response) => {
              this.setState({ loading: false })
              worker.isDeafEnabled = false;
              let newWorkers = JSON.parse(JSON.stringify(this.state.workers))
              this.state.workers.forEach((tempWorker, index) => {
                if (tempWorker._id == worker._id) {
                  newWorkers[index] = worker;
                }
              });

              this.setState({ workers: newWorkers });

            }).catch(error => this.setState({ loading: false }));
          } else {
            this.setState({ loading: true });
            axios.post('http://35.229.17.101:3989/unDeafConferenceurl', body).then((response) => {
              this.setState({ loading: false });
              worker.isDeafEnabled = true;
              let newWorkers = JSON.parse(JSON.stringify(this.state.workers))
              this.state.workers.forEach((tempWorker, index) => {
                if (tempWorker._id == worker._id) {
                  newWorkers[index] = worker;
                }
              });

              this.setState({ workers: newWorkers });

            }).catch(error => this.setState({ loading: false }));
          }
        }
        }

        onHeaderMicPress={() => {

          let memberIdList = [];
          let newWorkers = JSON.parse(JSON.stringify(this.state.workers));
          newWorkers.forEach((tempWorker) => {
            if (tempWorker.ConferenceMemberID) {
              memberIdList.push(tempWorker.ConferenceMemberID)
            }
          });

          let body = {
            confRoomName: this.state.userDataWithUUID,
            memberIdList: memberIdList
          }
          if (this.state.headerMicIcon) {
            this.setState({ loading: true });
            axios.post('http://35.229.17.101:3989/muteConferenceurl', body).then((response) => {
              this.setState({ loading: false })
              newWorkers.forEach((tempWorker) => {
                tempWorker.isMuteEnabled = false;
              });
              this.setState({ workers: newWorkers, headerMicIcon: false });

            }).catch(error => this.setState({ loading: false }));

          } else {
            this.setState({ loading: true });
            axios.post('http://35.229.17.101:3989/unMuteConferenceurl', body).then((response) => {
              this.setState({ loading: false })
              newWorkers.forEach((tempWorker) => {
                tempWorker.isMuteEnabled = true;
              });
              this.setState({ workers: newWorkers, headerMicIcon: true });
            }).catch(error => this.setState({ loading: false }));
          }

        }}

        onHeaderSpeakerPress={() => {

          let memberIdList = [];
          let newWorkers = JSON.parse(JSON.stringify(this.state.workers));
          newWorkers.forEach((tempWorker) => {
            if (tempWorker.ConferenceMemberID) {
              memberIdList.push(tempWorker.ConferenceMemberID)
            }
          });

          let body = {
            confRoomName: this.state.userDataWithUUID,
            memberIdList: memberIdList
          }

          if (this.state.headerSpeakerIcon) {
            this.setState({ loading: true });
            axios.post('http://35.229.17.101:3989/deafConferenceurl', body).then((response) => {
              this.setState({ loading: false })
              newWorkers.forEach((tempWorker) => {
                tempWorker.isDeafEnabled = false;
              });
              this.setState({ workers: newWorkers, headerSpeakerIcon: false });
            }).catch(error => this.setState({ loading: false }));
          } else {
            this.setState({ loading: true });
            axios.post('http://35.229.17.101:3989/unDeafConferenceurl', body).then((response) => {
              this.setState({ loading: false })
              newWorkers.forEach((tempWorker) => {
                tempWorker.isDeafEnabled = true;
              });
              this.setState({ workers: newWorkers, headerSpeakerIcon: true });
            }).catch(error => this.setState({ loading: false }));
          }

        }}

        onMessagePress={() => {
          console.log("onMessagePress");
          this.setState({ modalVisible: true })
        }}

        onCallPress={() => {

          if (this.socket)
            this.socket.disconnect();

          let contactList = [];

          contactList.push(this.state.loginData.contactNo)
          this.state.originalWorkers.forEach(worker => {
            contactList.push(worker.contactNo);
          });

          let body = {
            "contactList": contactList
          }
          this.setState({ loading: true, isCallLoadingVisible: true });

          // console.log("===>>>>" + JSON.stringify(body));

          axios.post('http://35.229.17.101:3989/startconference', body).then((response) => {
            AsyncStorage.removeItem('userDataWithUUID').then((reeee) => {
              AsyncStorage.setItem('userDataWithUUID', response.data.confRoomName).then((mmmm) => {
                // console.log("set userDataWithUUID response ===>>> 111" + JSON.stringify(response));
                this.setState({ loading: false })
                this.setState({ userDataWithUUID: response.data.confRoomName })
                this.connectSocketAndFetchData(response.data.confRoomName);
              }).catch((err) => { this.setState({ loading: false }) })
            }).catch((err) => { this.setState({ loading: false }) })
            // this.setState({ mandals: response.data.ConstituencyData[0].mandalList, constituencyData: response.data.ConstituencyData[0] })
          }).catch(error => this.setState({ loading: false }));
        }}

      />
    );
  }

  async connectSocketAndFetchData(userDataWithUUID) {
    // console.log("999999999999999999999");

    this.socket = SocketIOClient('http://35.229.17.101:3889');
    this.socket.on('connect', () => {
      // console.log("userDataWithUUID in connectSocketAndFetchData=== >>>" + userDataWithUUID);

      if (userDataWithUUID) {
        this.socket.emit('add user', userDataWithUUID);
        this.socket.emit('get all user', userDataWithUUID);
      }

    });

    this.socket.on('getAllUserResponse', (response) => {
      console.log("getAllUserResponse === >>" + JSON.stringify(response));
      // let modifiedWorkers = JSON.parse(JSON.stringify(this.state.workers));
      // let newWorkers = JSON.parse(JSON.stringify(this.state.workers));
      // response.forEach((outer, i) => {
      //   newWorkers.forEach((inner, j) => {
      //     if (outer.contactNo == inner.contactNo) {
      //       let obj = JSON.parse(JSON.stringify(inner));
      //       obj.ConferenceMemberID = outer.ConferenceMemberID;
      //       modifiedWorkers.splice(j, 1);
      //       modifiedWorkers.unshift(obj);
      //     }
      //   });
      // });

      let newResponse = JSON.parse(JSON.stringify(response));
      let newWorkers = JSON.parse(JSON.stringify(this.state.workers));

      console.log("======>>>" + JSON.stringify(newWorkers));

      if (response && response.length > 0) {

        response.forEach((e1) => newWorkers.forEach((e2) => {
          if (e1.contactNo === e2.contactNo) {

            e1._id = e2._id;
            e1.memberName = e2.memberName;
            e1.isDeafEnabled = e2.isDeafEnabled;
            e1.isMuteEnabled = e2.isMuteEnabled;
            e2.found = "true";
          }
        }
        ));

        newWorkers.forEach((element) => {
          if (element.found != "true") {
            if (element.ConferenceMemberID)
              delete element.ConferenceMemberID;
            response.push(element)
          }
        });

        let noOfConnected = 0;
        let noOfDisConnected = 0;
        if (response && response.length > 0) {
          noOfConnected = newResponse.length;
          noOfDisConnected = (response.length - newResponse.length);
        }

        this.setState({ workers: response, noOfConnected: noOfConnected, noOfDisConnected: noOfDisConnected, isCallLoadingVisible: true });
      } else {
        newWorkers.forEach(element => {
          if (element.ConferenceMemberID)
            delete element.ConferenceMemberID;
        });

        let noOfConnected = 0;
        let noOfDisConnected = newWorkers.length;

        this.setState({ workers: newWorkers, noOfConnected: noOfConnected, noOfDisConnected: noOfDisConnected, isCallLoadingVisible: false });
      }

    });

    this.socket.on('disconnect', (reason) => {

      console.log("Socket Disconnected ====>>>>>>>>>>>...<<<<<<<<<<<<<<<");

      // if (this.socket)
      //   this.socket.disconnect();
    });
  }


  componentDidMount() {
    this.subs = [
      this.props.navigation.addListener("didFocus", () => {

      }),
      this.props.navigation.addListener("willBlur", () => {
        if (this.socket)
          this.socket.disconnect();
      })
    ];
  }

  componentWillUnmount() {
    this.subs.forEach(sub => sub.remove());
  }

}

