import React, { Component } from "react";
import PropTypes from "prop-types";
import { Image, Alert, AsyncStorage } from "react-native";
import { Item, Input, Toast, Form } from "native-base";
import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";
import Login from "../../components/Login";
import { userLoginSuccess, userLoginFail } from "../../actions";
import { required, alphaNumeric } from "./validators";
import styles from "./styles";
import axios from 'axios';


const lockIcon = require("../../../assets/icon/lock.png");
const mailIcon = require("../../../assets/icon/mail.png");

class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      counter: 0,
      firstLoading: false
    };
  }

  componentWillReceiveProps(nextProps, nextState) {
    if (this.props.auth.isFailed !== nextProps.auth.isFailed) {
      if (nextProps.auth.isFailed && !nextProps.auth.isAuthenticating) {
        let message = nextProps.auth.error;
        setTimeout(() => {
          Alert.alert("", message);
        }, 100);
      }
    }

    if (this.props.auth.token !== nextProps.auth.token) {
      if (nextProps.auth.token) {
        this.props.navigation.navigate("App");
      }
    }
  }

  renderInput({ input, label, type, meta: { touched, error, warning } }) {
    return (
      <Item error={error && touched} style={styles.itemForm}>
        <Image source={input.name === "email" ? mailIcon : lockIcon} />
        <Input
          ref={c => (this.textInput = c)}
          placeholder={input.name === "email" ? "User Name" : "Password"}
          secureTextEntry={input.name === "password"}
          {...input}
          style={styles.inputText}
          keyboardType={input.name === "email" ? "email-address" : "default"}
          autoCapitalize="none"
        />
      </Item>
    );
  }

  login() {
    if (this.props.valid) {
      let { email, password } = this.props.loginForm.values;
      this.verifyUser(email, password);
    } else {
      Toast.show({
        text: "Enter Valid Username & password!",
        duration: 2000,
        position: "top",
        textStyle: { textAlign: "center" }
      });
    }
  }

  onPressSwitch() {
    let { counter } = this.state;
    this.setState({
      counter: counter + 1
    }, () => {
      if (this.state.counter === 8) {
        this.props.navigation.navigate("SwitchEnv");
      }
    });
  }

  verifyUser(mobileNo, password) {
    this.setState({ firstLoading: true });
    axios.get(`http://35.229.17.101:3789/boothmanagement/services/rest/verifyUser/${mobileNo}/${password}`).then((response) => {
      this.setState({ firstLoading: false });
      if (response.data && response.data.userData && response.data.userData.length > 0 && response.data.userData[0].authUser) {
        this.saveUserToDB(response.data.userData[0])
        this.props.loginSuccess({ mobileNo, password });
      } else {
        this.props.loginFail({ mobileNo, password });
      }
    }).catch(error => { this.setState({ firstLoading: false }); console.log(error) });
  }

  async saveUserToDB(userDataObj) {
    try {
      await AsyncStorage.setItem('userData', JSON.stringify(userDataObj));
    } catch (error) {
      // Error saving data
    }
  }

  render() {
    const form = (
      <Form>
        <Field
          name="email"
          component={this.renderInput}
          validate={[alphaNumeric, required]}
        />
        <Field
          name="password"
          component={this.renderInput}
          validate={[alphaNumeric, required]}
        />
      </Form>
    );
    return (
      <Login
        onPressSwitch={() => this.onPressSwitch()}
        navigation={this.props.navigation}
        loading={this.props.auth.isAuthenticating}
        firstLoading={this.state.firstLoading}
        loginForm={form}
        onLogin={() => this.login()}
      />
    );
  }
}

LoginForm.propTypes = {
  auth: PropTypes.object,
  loginForm: PropTypes.object,
  login: PropTypes.func
};

const LoginContainer = reduxForm({
  form: "login"
})(LoginForm);

const mapStateToProps = state => ({
  auth: state.auth,
  loginForm: state.form.login
});

const mapDispatchToProps = dispatch => ({
  loginSuccess: ({ email, password }) => dispatch(userLoginSuccess({
    email,
    password,
  })),
  loginFail: ({ email, password }) => dispatch(userLoginFail({
    email,
    password,
  })),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginContainer);
